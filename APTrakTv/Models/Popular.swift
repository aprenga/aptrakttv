//
//  Popular.swift
//  APTrakTv
//
//  Created by Alex on 9/2/16.
//  Copyright © 2016 Aleksander. All rights reserved.
//

import Foundation
import ObjectMapper

class Popular: Mappable {
    var title: String!
    var year: Int?
    var overview: String?
    var thumb: String?
    
    // MARK: - Protocol Conformance
    
    // MARK: Mappable
    required init?(_ map: Map) {
        guard let _ = map["title"].currentValue
            else {
            return nil
        }
    }
    
    func mapping(map: Map) {
        title    <- map["title"]
        year     <- map["year"]
        overview <- map["overview"]
        thumb    <- map["images.poster.thumb"]
    }
}
