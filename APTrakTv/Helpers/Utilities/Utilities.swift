//
//  Utilities.swift
//  APTrakTv
//
//  Created by Alex on 9/3/16.
//  Copyright © 2016 Aleksander. All rights reserved.
//

import Foundation
import UIKit

class Utilities {
    class func mainDarkColor () -> UIColor {
        return UIColor.darkGrayColor()
    }
    
    class func mainWhiteColor () -> UIColor {
        return UIColor.whiteColor()
    }
    
    class func mainGrayColor () -> UIColor {
        return UIColor.lightGrayColor()
    }
    
    class func createErrorAlertWithMessage (view: UIView, message: String) -> UIAlertController{
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .Alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { action in
        }
        alertController.addAction(cancelAction)
        
        if let presenter = alertController.popoverPresentationController {
            presenter.sourceView = view
            presenter.sourceRect = view.bounds
        }
        return alertController
    }
}

// Unwrap optional values

private protocol _Optional {
    func unwrappedString() -> String
}

extension Optional: _Optional {
    private func unwrappedString() -> String {
        switch self {
        case .Some(let wrapped as _Optional): return wrapped.unwrappedString()
        case .Some(let wrapped): return String(wrapped)
        case .None: return String(self)
        }
    }
}

postfix operator ~? { }
public postfix func ~? <X> (x: X?) -> String {
    return x.unwrappedString()
}

// MARK: - Global Appearance Setups

struct Appearance {
    static func setupNavigationBarStyle () {
        UINavigationBar.appearance().tintColor = Utilities.mainWhiteColor()
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: Utilities.mainWhiteColor()]
        UINavigationBar.appearance().barTintColor = Utilities.mainDarkColor()
        UIApplication.sharedApplication().statusBarStyle = .LightContent
    }
    
    static func setupSearchBarStyle () {
        UISearchBar.appearance().barTintColor = Utilities.mainDarkColor()
        UISearchBar.appearance().tintColor = Utilities.mainWhiteColor()
        (UITextField.appearanceWhenContainedInInstancesOfClasses([UISearchBar.self])).tintColor = Utilities.mainDarkColor()
    }
}
