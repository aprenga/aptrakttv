//
//  APIManager.swift
//  APTrakTv
//
//  Created by Alex on 9/2/16.
//  Copyright © 2016 Aleksander. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class APIManager {
    
    static var request: Alamofire.Request?
    
    class func getPopularMoviesForPage (page: Int, completion: (success: Bool, message: String, responseArray: [Popular]?) -> Void) {
        APIManager.getRequest(Constants.getPopularMoviesUrl(page), headers: UrlParameters.headers()) { (success, message, responseObject) -> Void in
            
            let response = Mapper<Popular>().mapArray(responseObject)
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if success {
                    completion(success: success, message: "", responseArray: response)
                } else {
                    completion(success: success, message: message, responseArray: response)
                }
            })
        }
    }
    
    class func searchMoviesWithType (type: Int, query: String, page: Int, completion: (success: Bool, message: String, responseArray: [Search]?) -> Void) {
        //Cancel the previous request when a new request comes
        if self.request != nil {
            self.request?.cancel()
        }
        APIManager.getRequest(Constants.getSearchMovieUrl(query, searchType: type, page: page), headers: UrlParameters.headers()) { (success, message, responseObject) -> Void in
            
            let response = Mapper<Search>().mapArray(responseObject)
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if success {
                    completion(success: success, message: "", responseArray: response)
                } else {
                    completion(success: success, message: message, responseArray: [])
                }
            })
        }
    }
}

// MARK: - Request Maker

extension APIManager {
    class func getRequest(url: String, headers: [String:String]?, completion:((success: Bool, message: String, responseObject: AnyObject?) -> Void)) {
        //Make the request
        self.request = Alamofire.request(.GET, url, parameters: nil, encoding: ParameterEncoding.JSON, headers: headers).responseJSON { response in
            switch response.result {
            case .Success(let data):
                guard let dataArray = data as? NSArray else {
                    completion(success: false, message: APIErrorMessage.Server.rawValue, responseObject: [])
                    return
                }
                //Got the object so send it back
                completion(success: true, message: APIErrorMessage.None.rawValue, responseObject: dataArray)
                break
                
            case .Failure(let err):
                print(err)
                completion(success: false, message: APIErrorMessage.Connection.rawValue, responseObject: [])
                break
            }
        }
    }
}