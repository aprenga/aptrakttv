//
//  PopularMoviesViewController.swift
//  APTrakTv
//
//  Created by Alex on 9/2/16.
//  Copyright © 2016 Aleksander. All rights reserved.
//

import Foundation
import UIKit
import ESPullToRefresh
import DZNEmptyDataSet

class PopularMoviesViewController: UIViewController {
    //Outlets
    @IBOutlet weak var tableView: UITableView!
    //Vars
    var searchController = UISearchController()
    var popularArray: [Popular] = []
    var searchArray: [Search] = []
    var popularPageIndex = 0
    var searchPageIndex = 0
    var isControllerInitialised = false
    var shouldDisplayEmptyData = false
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Setup Table View
        setuptableView()
        //Setup Views
        setupViews()
        //Setup Search Bar
        setupSearchController()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if !isControllerInitialised {
            tableView.es_startPullToRefresh()
            isControllerInitialised = true
        }
    }
    
    // MARK: - Deinitialization Methods
    
    deinit {
        // UISearchController attempts to load its view in its dealloc method, so we force it to load its view
        searchController.loadViewIfNeeded()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Setups
    
    func setupViews () {
        self.title = "Popular Movies"
    }
    
    func setuptableView () {
        tableView.delegate = self
        tableView.dataSource = self
        
        //Empty Data Set
        tableView.emptyDataSetSource = self;
        tableView.emptyDataSetDelegate = self;
        
        //Graphic details
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.clipsToBounds = true
        
        //Add pull to refresh
        addPullToRefresh()
        
        //Table view pull to refresh settings
        tableView.refreshIdentifier = String(PopularMoviesViewController.self)
        tableView.expriedTimeInterval = 20.0
        
        addLoadMore()
    }
    
    //Setup search controller
    func setupSearchController () {
        searchController = UISearchController(searchResultsController: nil)
        
        self.definesPresentationContext = true
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        
        searchController.searchBar.delegate = self
        tableView.tableHeaderView = searchController.searchBar
        
        searchController.searchBar.sizeToFit()
        searchController.searchBar.placeholder = "Search Movie"
        
        searchController.searchBar.autocapitalizationType = .None
        
        //Setup search controller appearance
        Appearance.setupSearchBarStyle()
        
        // Add scopes
        searchController.searchBar.scopeButtonTitles = ["Movie", "Show", "Episode", "Person", "List"]
        searchController.searchBar.showsScopeBar = false;
    }
}

// MARK: - UITableView Delegate and Datasource

extension PopularMoviesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: PopularTableViewCell
        
        //Load data & configure cell for each table
        if searchController.active {
            cell = tableView.dequeueReusableCellWithIdentifier(PopularTableViewCell.searchCellIdentifier, forIndexPath: indexPath) as! PopularTableViewCell
            //Load data & configure cell
            let model = searchArray[indexPath.row]
            //Configure cell
            cell.configureSearchCell(model)
        } else {
            cell = tableView.dequeueReusableCellWithIdentifier(PopularTableViewCell.popularCellIdentifier, forIndexPath: indexPath) as! PopularTableViewCell
            //Load data & configure cell
            let model = popularArray[indexPath.row]
            //Configure cell
            cell.configurePopularCell(model)
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if searchController.active {
            let movie = searchArray[indexPath.row]
            print("\(movie.title) was selected")
        } else {
            let movie = popularArray[indexPath.row]
            print("\(movie.title) was selected")
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (searchController.active) ? searchArray.count : popularArray.count
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

// MARK: - Pull To Refresh & Load More Methods

extension PopularMoviesViewController {
    func addPullToRefresh () {
        tableView.es_addPullToRefresh {
            [weak self] in
            if ((self?.searchController)!.active) {
                self!.searchArray.removeAll()
                self?.searchPageIndex = 0
                self?.searchMovieWithQuery((self?.searchController.searchBar.text)!, type: self!.searchController.searchBar.selectedScopeButtonIndex)
            } else {
                // When pulling for refresh start from the first page (page 0 & 1 returns the same results from the API but I am starting from page 0 because this is how it is supposed to work)
                self?.popularPageIndex = 0
                APIManager.getPopularMoviesForPage(self!.popularPageIndex, completion: { (success, message, responseArray) in
                    self!.shouldDisplayEmptyData = true
                    if success {
                        self?.popularArray.removeAll()
                        if let response = responseArray {
                            self?.popularArray = response
                        }
                        self?.popularPageIndex += 1
                    } else {
                        self?.presentViewController(Utilities.createErrorAlertWithMessage(self!.view, message: message), animated: true, completion: nil)
                    }
                    self?.tableView.reloadData()
                    self?.tableView.es_stopPullToRefresh(completion: true)
                })
            }
        }
    }
    
    func addLoadMore () {
        tableView.es_addInfiniteScrolling {
            [weak self] in
            if ((self?.searchController)!.active) {
                // Don't activate load more if there are no elements in the table view
                if self?.searchArray.count == 0 {
                    self?.tableView.es_stopLoadingMore()
                } else {
                    self?.searchMovieWithQuery((self?.searchController.searchBar.text)!, type: self!.searchController.searchBar.selectedScopeButtonIndex)
                }
            } else {
                // Don't activate load more if there are no elements in the table view
                if self?.popularArray.count == 0 {
                    self?.tableView.es_stopLoadingMore()
                } else {
                    APIManager.getPopularMoviesForPage(self!.popularPageIndex, completion: { (success, message, responseArray) in
                        if success {
                            if responseArray?.count > 0 {
                                self?.popularArray.appendContentsOf(responseArray!)
                                self?.popularPageIndex += 1
                            } else {
                                self?.tableView.es_noticeNoMoreData()
                            }
                        } else {
                            self?.presentViewController(Utilities.createErrorAlertWithMessage(self!.view, message: message), animated: true, completion: nil)
                        }
                        self?.tableView.reloadData()
                        self?.tableView.es_stopLoadingMore()
                    })
                }
                
            }
        }
    }
}

//Empty Data Methods

extension PopularMoviesViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    //Datasource Methods
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "movies")
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let text = APIErrorMessage.NoResults.rawValue
        let attributes = [NSFontAttributeName: UIFont.boldSystemFontOfSize(18.0), NSForegroundColorAttributeName: Utilities.mainDarkColor()]
        return NSAttributedString(string: text, attributes: attributes)
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        if !searchController.active {
            let text = APIErrorMessage.TryAgain.rawValue
            let paragraph = NSMutableParagraphStyle()
            paragraph.lineBreakMode = .ByWordWrapping
            paragraph.alignment = .Center
            let attributes = [NSFontAttributeName: UIFont.systemFontOfSize(14.0), NSForegroundColorAttributeName: Utilities.mainGrayColor(), NSParagraphStyleAttributeName: paragraph]
            return NSAttributedString(string: text, attributes: attributes)
        }
        return nil
    }
    
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView!, forState state: UIControlState) -> NSAttributedString! {
        if !searchController.active {
            let text = "Retry"
            let attributes = [NSFontAttributeName: UIFont.boldSystemFontOfSize(17.0), NSForegroundColorAttributeName: Utilities.mainDarkColor()]
            return NSAttributedString(string: text, attributes: attributes)
        }
        return nil
    }
    
    func backgroundColorForEmptyDataSet(scrollView: UIScrollView!) -> UIColor! {
        return UIColor.whiteColor()
    }
    
    //Delegate Methods
    func emptyDataSetShouldDisplay(scrollView: UIScrollView!) -> Bool {
        return shouldDisplayEmptyData
    }
    
    func emptyDataSetShouldAllowTouch(scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowScroll(scrollView: UIScrollView!) -> Bool {
        return false
    }
    
    func emptyDataSetShouldAnimateImageView(scrollView: UIScrollView!) -> Bool {
        return false
    }
    
    func emptyDataSet(scrollView: UIScrollView!, didTapButton button: UIButton!) {
        tableView.es_startPullToRefresh()
    }
}

// MARK: - Search Bar Methods

extension PopularMoviesViewController: UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating {
    
    // MARK: - UISearchController Delegate Methods
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        // Reload data when searchController becomes active because of an issue with search controller
        tableView.reloadData()
        if searchController.searchBar.text != "" {
            NSObject.cancelPreviousPerformRequestsWithTarget(self, selector: #selector(throttleSearchUpdate), object: nil)
            self.performSelector(#selector(throttleSearchUpdate), withObject: nil, afterDelay: 0.5)
        } else {
            searchArray.removeAll()
            tableView.es_resetNoMoreData()
            tableView.reloadData()
        }
    }
    
    func didPresentSearchController(searchController: UISearchController) {
        searchController.searchBar.showsScopeBar = true;
    }
    
    func didDismissSearchController(searchController: UISearchController) {
        searchController.searchBar.showsScopeBar = false;
        searchPageIndex = 0
        searchArray.removeAll()
        tableView.reloadData()
    }
    
    // MARK: - Search Bar Delegate Methods
    
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        searchArray.removeAll()
        // Reload data in order show empty data
        tableView.reloadData()
        if searchController.searchBar.text != "" {
            searchPageIndex = 0
            searchMovieWithQuery(searchController.searchBar.text!, type: selectedScope)
        }
    }
    
    // MARK: - Search Network Services
    
    func throttleSearchUpdate () {
        if searchController.searchBar.text != "" {
            searchArray.removeAll()
            searchPageIndex = 0
            searchMovieWithQuery(searchController.searchBar.text!, type: searchController.searchBar.selectedScopeButtonIndex)
        }
    }
    
    func searchMovieWithQuery (query: String, type: Int) {
        APIManager.searchMoviesWithType(type, query: query, page: searchPageIndex) { (success, message, responseArray) in
            if success {
                if let response = responseArray {
                    self.searchArray.appendContentsOf(response)
                }
                self.searchPageIndex += 1
                // Means that this call is triggered by the load more in the search table
                if self.searchPageIndex > 0 {
                    if responseArray?.count == 0 {
                        self.tableView.es_stopLoadingMore()
                    }
                    self.tableView.es_noticeNoMoreData()
                }
            } else {
                self.presentViewController(Utilities.createErrorAlertWithMessage(self.view, message: message), animated: true, completion: nil)
            }
            self.tableView.reloadData()
            self.tableView.es_stopPullToRefresh(completion: true)
        }
    }
}





