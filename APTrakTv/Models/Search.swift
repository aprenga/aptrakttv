//
//  Search.swift
//  APTrakTv
//
//  Created by Alex on 9/2/16.
//  Copyright © 2016 Aleksander. All rights reserved.
//

import Foundation
import ObjectMapper

class Search: Mappable {
    var type: String!
    var title: String!
    var year: Int?
    var overview: String!
    var thumb: String!
    
    // MARK: - Protocol Conformance
    
    // MARK: Mappable
    required init?(_ map: Map) {
        guard let mapType = map["type"].currentValue,
        let _ = map["\(mapType).title"].currentValue,
        let _ = map["\(mapType).overview"].currentValue,
        let _ = map["\(mapType).images.poster.thumb"].currentValue
        else {
            return nil
        }
    }
    
    func mapping(map: Map) {
        type     <- map["type"]
        title    <- map["\(type).title"]
        year     <- map["\(type).year"]
        overview <- map["\(type).overview"]
        thumb    <- map["\(type).images.poster.thumb"]
    }
}
