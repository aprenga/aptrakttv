//
//  PopularTableViewCell.swift
//  APTrakTv
//
//  Created by Alex on 9/2/16.
//  Copyright © 2016 Aleksander. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class PopularTableViewCell: UITableViewCell {
    // Outlets
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieYearLabel: UILabel!
    @IBOutlet weak var movieOverviewLabel: UILabel!
    
    // Constants
    static let popularCellIdentifier = "popularCell"
    static let searchCellIdentifier = "searchCell"
    
    // MARK: - Configuring Methods
    
    func configurePopularCell (movie: Popular) {
        movieTitleLabel.text = movie.title
        movieYearLabel.text = "\(movie.year~?)"
        if let imageUrl = movie.thumb {
            movieImageView.sd_setImageWithURL(NSURL(string: imageUrl), placeholderImage: UIImage(named: "movies"), completed: nil)
        } else {
            movieImageView.image = UIImage(named: "movies")
        }
    }
    
    func configureSearchCell (movie: Search) {
        movieTitleLabel.text = movie.title
        movieYearLabel.text = "\(movie.year~?)"
        movieOverviewLabel.text = "\(movie.overview)"
        if let imageUrl = movie.thumb {
            movieImageView.sd_setImageWithURL(NSURL(string: imageUrl), placeholderImage: UIImage(named: "movies"), completed: nil)
        } else {
            movieImageView.image = UIImage(named: "movies")
        }
    }
}
