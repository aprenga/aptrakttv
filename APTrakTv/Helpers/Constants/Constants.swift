//
//  Constants.swift
//  APTrakTv
//
//  Created by Alex on 9/2/16.
//  Copyright © 2016 Aleksander. All rights reserved.
//

import Foundation

class Constants: NSObject {
    
    static let kTraktApiKey = "ad005b8c117cdeee58a1bdb7089ea31386cd489b21e14b19818c91511f12a086"
    static let kBaseUrl = "https://api.trakt.tv/"
    
    class func getPopularMoviesUrl (page: Int) -> String {
        return Constants.kBaseUrl.stringByAppendingString("movies/popular?page=\(page)&limit=10&extended=full,images")
    }
    
    class func getSearchMovieUrl (query: String, searchType: Int, page: Int) -> String {
        let type = SearchType(rawValue: searchType)!.string
        return Constants.kBaseUrl.stringByAppendingString("search/\(type)?query=\(query)&page=\(page)&limit=10&extended=full,images").stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
    }
}

struct UrlParameters {
    static func headers () -> Dictionary<String, String> {
        return ["Content-Type": "application/json",
                "trakt-api-version": "2",
                "trakt-api-key": Constants.kTraktApiKey]
    }
}