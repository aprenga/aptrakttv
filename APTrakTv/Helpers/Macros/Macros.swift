//
//  Macros.swift
//  APTrakTv
//
//  Created by Alex on 9/2/16.
//  Copyright © 2016 Aleksander. All rights reserved.
//

import Foundation

enum SearchType: Int {
    case movie
    case show
    case episode
    case person
    case list
    
    var string: String {
        return String(self)
    }
}

enum APIErrorMessage: String {
    case Connection = "APTraktTV couldn't connect to server",
    Server = "A problem occurred",
    Response = "Something went wrong",
    NoResults = "There is nothing to display here",
    TryAgain = "Please try again later",
    None = "Success"
    
    func toString() -> String {
        return self.rawValue
    }
}
